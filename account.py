#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Bool, And
from trytond.pool import Pool


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    party_is_mandatory = fields.Boolean('Mandatory Party',
            states={'invisible': Equal(Eval('kind'), 'view')}, depends=['kind'],
            help="Check, if party must be required in account move lines " \
            "which make use of this account",)

    def default_party_is_mandatory(self):
        return False

Account()


class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    party_is_mandatory = fields.Function(fields.Boolean('Mandatory Party',
            on_change_with=['account']), 'get_party_is_mandatory')

    def __init__(self):
        super(Line, self).__init__()
        self.party = copy.copy(self.party)
        if 'party_is_mandatory' not in self.party.depends:
            self.party.depends = copy.copy(self.party.depends)
            self.party.depends.append('party_is_mandatory')
        if not self.party.states:
            self.party.states = {'required':
                    Bool(Eval('party_is_mandatory'))}
        else:
            if 'required' not in self.party.states:
                self.party.states.update({'required':
                        Bool(Eval('party_is_mandatory'))})
            else:
                self.party.states['required'] = \
                    And(self.party.states['required'],
                        Bool(Eval('party_is_mandatory')))
        self._reset_columns()

    def get_party_is_mandatory(self, ids, name):
        res = {}
        for line in self.browse(ids):
            res[line.id] = line.account.party_is_mandatory
        return res

    def on_change_with_party_is_mandatory(self, vals):
        account_obj = Pool().get('account.account')

        res = False
        if vals.get('account'):
            account = account_obj.browse(vals['account'])
            res = account.party_is_mandatory
        return res

Line()


class AccountTemplate(ModelSQL, ModelView):
    _name = 'account.account.template'

    party_is_mandatory = fields.Boolean('Mandatory Party',
            states={'invisible': Equal(Eval('kind'), 'view'),}, depends=['kind'],
            help="Check, if party must be required in account move lines " \
            "which make use of this account",)

    def default_party_is_mandatory(self):
        return False

    def _get_account_value(self, template, account=None):
        res = super(AccountTemplate, self)._get_account_value(template,
                account=account)

        if not account or account.party_is_mandatory != \
                template.party_is_mandatory:
            res['party_is_mandatory'] = template.party_is_mandatory
        return res

AccountTemplate()

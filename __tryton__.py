#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Party Required',
    'name_de_DE': 'Buchhaltung Partei Erforderlich',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://virtual-things.biz',
    'description': '''Account Option Party Required
    - Adds the option field 'Mandatory Party' to accounts

    Using accounts with checked field 'Mandatory Party' in move lines will set
    the party field required for this move line.
''',
    'description_de_DE': '''Buchhaltung Kontenoption 'Partei erforderlich'
    - Fügt den Konteneinstellungen die  Option 'Partei erforderlich' hinzu

    Wird ein Konto mit dieser Option in einer Buchungszeile verwendet, wird die
    Angabe einer Partei für diese Buchungszeile erforderlich.
''',
    'depends': [
        'account',
    ],
    'xml': [
        'account.xml',
    ],
    'translation': [
         'locale/de_DE.po',
    ],
}
